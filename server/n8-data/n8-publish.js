Actions = new Mongo.Collection("actions");
Messages = new Mongo.Collection('messages');
User = new Mongo.Collection('user');
Received = new Mongo.Collection('received');
Marks = new Mongo.Collection('marks');

if (Meteor.isServer) {
    Meteor.publish('events', function() {
        var self = this;

        // Some very interesting books
        var events = n8.events;

        _.each(events, function(event) {
            self.added('events', Random.id(), event);
        });

        self.ready();
    });

    Meteor.publish('locations', function() {
        var self = this;

        // Some very interesting books
        var locations = n8.musea;

        _.each(locations, function(location) {
            self.added('locations', Random.id(), location);
        });

        self.ready();
    });

    Meteor.publish('actions', function(){

        var count = Actions.find().count();

        // Insert into Mongo if it does not exist yet
        if(!count) {
            console.log('insert');

            var actions = actionsJS;

            _.each(actions, function(action) {
                Actions.insert(action);
            });
        }

        return Actions.find({});

    });

    Meteor.publish('user', function(){

        return User.find({});

    });
    //
    //Meteor.publish('user', function(user_id){
    //
    //    return User.find({_id: user_id});
    //
    //});

    Meteor.publish('received', function(){

        return Received.find({});

    });

    Meteor.publish('marks', function(){

        return Marks.find({});

    });

}