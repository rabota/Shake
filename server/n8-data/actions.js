actionsJS = {
    "1": {
        "name": "Electric taxi 15% off",
        "title": "Electric taxi 15% off",
        "short": "Get a cheaper ride in our sustainable taxi.",
        "image": {
            file: "car.jpg"
        },
        "thumbnail": {
            file: "car.jpg"
        },
        "text": "<p>Woon je in Amsterdam-Noord en wil je naast alle toffe locaties in het centrum ook naar Zuid-Oost om een kip te ontleden in anatomie Museum Vrolik? Of start jij je Museumnacht in de Funkkaravaan bij Huize Frankendael in Oost maar móét je ook echt even een kijkje nemen bij het Van Eesterenmuseum in West? Fear not! Wij hebben de oplossing voor je. Tijdens Museumnacht kan je je op vertoon van je polsbandje met 15% korting volledig duurzaam, groen en electrisch van museum naar museum bewegen. Want N8 houdt van deze wereld en ziet onze poolkappen graag bedekt met ijs! Heb jij je route nu al uitgestippeld en wil je om 17:45 zo'n groene bolide klaar hebben staan om je naar NEMO te brengen voor de openingsperformance?</p>",
        "response": {
            "yes": {
                "title": "Yeaaaa! See you in a bit",
                "description": "Various places"
            },
            "no": {
                "title": "Aaah! To bad"
            }
        },
        "geo": {
            "location": "de-balie",
            "lat": "52.3630846",
            "lon": "4.883435400000053"
        }
    },
    "2": {
        "name": "Roller Disco discount",
        "title": "Roller Disco after party discount",
        "image": {
            file: "disco.jpg"
        },
        "thumbnail": {
            file: "disco.jpg"
        },
        "short": "Dance on your roller skates, baby!",
       "text": "<p>De kaartjes zijn aan de deur 10 euro in plaats van 12 euro, op vertoon van je Museumnacht polsbandje! Dance on your roller skates, baby! Na een succesvolle eerste editie vorig jaar is het tijd voor een vervolg! Donker Collective, Joyriders en Blender slaan de handen ineen om tijdens museumnacht De Balie Amsterdam om te toveren tot een ware rollerdisco. Vanaf 22:00 tot 02:00 uur kan je komen skaten bij ons. Daarna gaan we nog door tot 07:00 uur met een heerlijk feestje. Als je niet in het bezit bent van skates, maak je geen zorgen! Wij hebben deze op de avond zelf natuurlijk voor jullie! </p>",
        "response": {
            "yes": {
                "title": "Yeaaaa! See you in a bit",
                "description": "De Balie, 22:00–6:00"
            },
            "no": {
                "title": "Aaah! To bad"
            }
        },
        "geo": {
            "location": "de-balie",
            "lat": "",
            "lon": ""
        }
    },
    "3": {
        "name": "Free gift, drinks and nibbles!",
        "title": "Free gift, drinks and nibbles!",
        "image": {
            file: "100.jpg"
        },
        "thumbnail": {
            file: "100.jpg"
        },
        "short": "This is a sample message that crosses over in two lines and goes on for a while.",
        "text": "<p>Het Persmuseum bestaat 100 jaar, en om die reden wordt tijdens Museumnacht elke honderdste bezoeker verrast met een gratis drankje, hapje en een klein cadeautje.</p>",
        "response": {
            "yes": {
                "title": "Yeaaaa! See you in a bit",
                "description": "At the entrance of the Persmuseum"
            },
            "no": {
                "title": "Aaah! To bad"
            }
        },
        "geo": {
            "location": "Persmuseum",
            "lat": "52.3691876",
            "lon": "4.9377001"
        }
    },
    "4": {
        "name": "Visit again for free",
        "title": "Visit again for free",
        "image": {
            file: "visit.jpg"
        },
        "thumbnail": {
            file: "visit.jpg"
        },
        "short": "This is a sample message that crosses over in two lines and goes on for a while.",
       "text": "<p>Na de Museumnacht mag je van ons gratis nog een keer terug naar je favoriete museum! Je doet dit door je polsbandje te bewaren. Maak je geen zorgen, afgeknipt mag ook. Dan kun je op je gemak nog eens terug naar dat museum waar het zo gezellig was, om nu zoder drukte te zien wat er zo mooi was. Of het museum bezoeken dat nog op je lijstje stond maar waar je geen tijd meer voor had tijdens de nacht. Geen idee waar je heen moet? We hebben we zelf ook nog eens vier hele leuke evenementen georganiseerd waar je gratis heen mag. Namelijk: Drie Keer Kijken in De Appel, De Detox Tour in de Hortus Botanius, Let’s Get Spanish in de Hermitage, en De Analyse in Museum Van Loon. Of je kunt de Nachtbrakers volgen voor leuke tips en evenementen. En neem vooral ook een gratis Bijlage mee tijdens de Museumnacht om de agenda lekker uit te pluizen. Deze actie loopt t/m 31 december 2015.</p>",
        "response": {
            "yes": {
                "title": "Yeaaaa! See you in a bit",
                "description": "See you again soon"
            },
            "no": {
                "title": "Aaah! To bad"
            }
        },
        "geo": {
            "location": "",
            "lat": "",
            "lon": ""
        }
    },
    "5": {
        "name": "Limited edition posters",
        "title": "Limited edition posters",
        "image": {
            file: "poster.jpg"
        },
        "thumbnail": {
            file: "poster.jpg"
        },
       "text": "<p>&WOLF is het online platform voor betaalbare kunst met gevarieerde collecties voor zowel de beginnende kunstliefhebber als de doorgewinterde verzamelaar. Ze werken samen met jonge, opkomende kunstenaars om supermooie werken betaalbaar te maken zodat jij ze gemakkelijk in huis kan halen. We hebben dus allebei wel een dingetje met toegankelijkheid!</p>",
        "short": "This is a sample message that crosses over in two lines and goes on for a while.",
        "response": {
            "yes": {
                "title": "Yeaaaa! See you in a bit",
                "description": "At the entrance of the Rijksmuseum"
            },
            "no": {
                "title": "Aaah! To bad"
            }
        },
        "geo": {
            "location": "",
            "lat": "",
            "lon": ""
        }
    }
}