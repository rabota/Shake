UserGround = new Ground.Collection('user', { connection: null });
User = new Mongo.Collection('user');

Template.shake.rendered = function() {

    createUUID();

};

Template.marker.rendered = function() {

    createUUID();

};

function createUUID() {

    var userID = UserGround.findOne();

    if(userID) {

        // add to DB
        var user = User.findOne({uuid: userID.uuid});

        if(!user) {
            User.insert({uuid: userID.uuid});
        }

        var user = userID;

    } else {

        // create random ID and add to DB
        var randomID = Random.id();

        UserGround.insert({uuid: randomID});
        User.insert({uuid: randomID});

        var user = randomID;

    }

    Session.set('user', user)

}