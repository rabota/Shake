Router.configure({
    //layoutTemplate: 'masterLayout',
    //notFoundTemplate: 'notFound',
    loadingTemplate: 'loading',
});

Router.map(function() {

    this.route('home', {

        path: '/',
        waitOn: function () {
            Meteor.subscribe('user')
            Meteor.subscribe('received')
            Meteor.subscribe('marks')
            return Meteor.subscribe('events')
        },
        data: function () {
            return Events.find().fetch();
        },
        action: function () {
            if (this.ready()) { this.render('shake'); }
            //else
            //   this.render('shake'); // loading
        }

    });

    this.route('dashboard', {

        path: '/dashboard',
        waitOn: function () {
            return Meteor.subscribe('locations')
        },
        data: function () {
            return Locations.find().fetch();
        },
        action: function () {
            if (this.ready())
                this.render('dashboard');
            //else
            //   this.render('shake'); // loading
        }

    });

    this.route('mark', {

        path: '/mark',
        waitOn: function () {
            return Meteor.subscribe('marks')
        },
        data: function () {
            return Marks.find().fetch();
        },
        action: function () {
            if (this.ready())
                this.render('marker');
            //else
            //   this.render('shake'); // loading
        }

    });

});