Marks = new Mongo.Collection('marks');
Meteor.subscribe('marks');

Template.marker.events({
    'click .mark': function(event) {

        event.preventDefault();

        // get geo
        var location = Geolocation.currentLocation();

        if(location) {

            var userLat = location.coords.latitude;
            var userLong = location.coords.longitude;

            var user = Session.get('user');

            Marks.insert({lat: userLat, lon: userLong, user: user.uuid});

        }

    }
});