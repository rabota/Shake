Session.setDefault('single', false);

Template.suggestion.helpers({
    list: function () {

        return Session.get('list');

    },
    single: function () {

        return Session.get('single');

    }
});

Template.list_item.events({
    'click .open-single': function () {

        var list = Session.get('list');
        Session.set('single', this);

    }
});

// ShakeIt Library
ShakeIt = {

    suggestion: function() {

        var single = Session.get('single');

        if(single) {

            console.log(single);
            this.createSingle();

        } else {

            console.log('list');
            this.createList();

        }

    },
    getAction: function() {

        var user = Session.get('user');

        var show_action = false;

        // Check if the user already received the action
        var actions = Actions.find({send: true});

        actions.forEach(function (action) {

            var received =  Received.find({user: user.uuid, action: action._id}).count();

            if(!received) show_action = action;

        });

        if(show_action) {

            // Set to received
            Received.insert({user: user.uuid, action: show_action._id});

            // Return action
            return Actions.findOne(show_action._id);

        }

    },
    createSingle: function() {

        var currentList = Session.get('list'),
            single = Session.get('single');

        // Current index
        var currentId = single._id;
        var index = _.indexOf(_.pluck(currentList, '_id'), currentId);

        // Skip to next in list
        index++;
        var next = currentList[index];

        // If last in list -> create new list
        if(!next) {
            currentList = this.createList();
            var next = currentList[0];
        }

        Session.set('single', next);

    },
    createList: function() {

        var list = [],
            items = 5;;

        var events = Events.find().fetch();

        // Action
        var action = this.getAction();

        for (i = 0; i < items; i++) {

            // Find random event
            var rand = Math.floor(Math.random()*events.length);

            if(action && i == 2) {
                // Inject action
                list.push(action);
            } else {
                // Add event
                list.push(events[rand]);
            }

            console.log(events[rand]);

        }

        Session.set('list', list);

        return list;

    }

}