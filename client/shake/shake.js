Session.setDefault('watching', true);
Session.setDefault('shakesCount', 0);
Session.setDefault('sensitivity', 15);
Session.setDefault('action', false);

Events = new Mongo.Collection('events');
Received = new Mongo.Collection('received');

// Template helper
Template.shake.helpers({
    shaked: function () {

        return Session.get('shakesCount');

    }
});

// Template events
Template.shake.events({

    'click .fakeShake': function() {

        Session.set('shakesCount', Session.get('shakesCount') + 1);
        ShakeIt.suggestion();

    }

});

// Track shake event
onShake = _.debounce(function onShake() {

    Session.set('shakesCount', Session.get('shakesCount') + 1);
    ShakeIt.suggestion();

}, 750, true);

// Start shake tracking
Meteor.startup(function () {
    if (shake && typeof shake.startWatch === 'function') {
        shake.startWatch(onShake, Session.get('sensitivity'));
    } else {
        console.log('Shake not supported');
    }
});