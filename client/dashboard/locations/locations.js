Locations = new Mongo.Collection('locations');
Meteor.subscribe('locations');

Session.setDefault('locations-selected', []);

Template.locations.helpers({
    locations_busy: function() {
        var locations = getLocations();

        return locations[0];

    },
    locations_okay: function() {
        var locations = getLocations();

        return locations[1];

    },
    locations_no_worries: function() {
        var locations = getLocations();

        return locations[2];

    }
});

function getLocations() {

    var locations = Locations.find().fetch();

    var n = 14;
    var lists = _.groupBy(locations, function(element, index){
        return Math.floor(index/n);
    });
    lists = _.toArray(lists);

    return lists;

}

Template.location.helpers({
    marked: function() {

        var location = this.coords;
        var name = this.title;
        var state = false;

        var marked = Marks.find();

        marked.forEach(function (mark) {

            var check = Geo.distance(mark.lat, mark.lon, location.lat, location.lon, "K");

            if(check < 1) {
                state = true;
            }

        });

        if(state) return true;

    },
    random: function() {
        return Math.floor((Math.random() * 240) + 40);
    }
});

Template.location.events({
    'click .select-location': function() {

        var action = Session.get('active-action');

    }
});