Template.dashboard.helpers({
    show_statistics: function() {
        return Session.get('active-action');
    }
});

Template.dashboard.rendered = function () {
    $('.collapsible').collapsible({
        accordion : false
    });
}