Actions = new Mongo.Collection('actions');
Meteor.subscribe('actions');

Template.actions.helpers({
    actions: function() {
        return Actions.find();
    }
});

Template.actions.events({
    'click .open-action': function(event) {
        event.preventDefault();
        Session.set('active-action', this._id);
    },
    'click .submit': function() {

        var state = Actions.findOne(this._id);
        var setState = !state.send;

        if(!setState) {
            // clear users
            Meteor.call('removeReceivedState', this._id);
        }

        Actions.upsert(this._id, { $set: {send: setState} });

    }
});

Template.action.helpers({
    selected: function() {
        var active = Session.get('active-action');

        if(active == this._id) return 'active';

    }
});