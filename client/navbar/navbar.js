// Template navbar
Template.navbar.helpers({

    single: function() {

        return Session.get('single');

    }

});

// Template navbar
Template.navbar.events({

    'click .list-view': function() {

        Session.set('single', '');

    }

});